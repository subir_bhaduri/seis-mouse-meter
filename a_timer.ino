//-------------------------------------------------------------------------------------------------------------
// Timer1 setup

// Fixed sampling frequency required.
// Prescaler used to reduce 16MHz to smaller values
// Timer1 tics cutoff level
// as per OCR1A formula here:  https://nerd-corner.com/arduino-timer-interrupts-how-to-program-arduino-registers/
// OCR1A =  CPU speed / prescaler * sampling period - 1 = 16Mhz / 64 * 0.010s -1  = 1249
const uint16_t number_of_timer1_ticks = 16000000.0/64.0 * sampling_period_ms/1000.0 - 1.0;

void init_auto_sampling() {

  // disable global interrupts
  cli();

  // Turn on CTC mode.
  // from ref: "For the time controlled pulse you need the so called “CTC Mode.
  // In CTC mode (“Clear Timer on Compare Mode”) the counter is cleared when the value of the counter (TNCT1)
  // matches either the value of the OCR1A register or the value of the ICR1 register (in our case OCR1A).
  // So the OCR1A register determines the maximum value of the counter and thus its resolution.
  TCCR1A = 0;               // set entire TCCR1A register to 0
  TCCR1B = 0;               // same for TCCR1B
  TCCR1B |= (1 << WGM12);   // ?

  // Set CS10 and CS11 bits for 64 prescaler:
  TCCR1B |= (1 << CS10);
  TCCR1B |= (1 << CS11);

  // set compare match register to desired timer count:
  OCR1A = number_of_timer1_ticks;

  // enable timer compare interrupt:
  TIMSK1 |= (1 << OCIE1A);

  // enable global interrupts
  sei();
}

// The timer ISR that triggers everytime Timer1 crosses the set number of ticks
ISR(TIMER1_COMPA_vect) {

  // If mouse has moved then pass, else reset the dx, dy from previous times to eliminate error in the cumulative sums x and y
  if (movement_flag == false) {
    dx = 0;
    dy = 0;
  }

  // Add the increments from the mouse to the overall position of the image.
  x = x + dx ;
  y = y + dy ;

  // Remove the drift using exponential moving average high pass filter
  //  from: https://www.norwegiancreations.com/2016/03/arduino-tutorial-simple-high-pass-band-pass-and-band-stop-filtering/
  EMA_S_x = (EMA_a * float(x)) + ((1 - EMA_a) * EMA_S_x);      //run the EMA
  x = x - EMA_S_x;                                      //calculate the high-pass signal
  EMA_S_y = (EMA_a * float(y)) + ((1 - EMA_a) * EMA_S_y);      //run the EMA
  y = y - EMA_S_y;                                      //calculate the high-pass signal

  x_buffer[buffer_number][buffer_count] = x;
  y_buffer[buffer_number][buffer_count] = y;
  //snprintf(xy_buffer[buffer_number][buffer_count], BUFFLEN,"%d,%d",x,y);
  //Serial.println(xy_buffer[buffer_number][buffer_count]);
  Serial.println(y);

  buffer_count++;

  if (buffer_count >= buffer_len) {
    buffer_count = 0;
    
    flag_buffer_ready = true;
    buffer_number_to_SD = buffer_number;

    if(buffer_number == 0) buffer_number = 1 ;
    else buffer_number = 0 ;
  }
  
  // Reset flag to false. this flag is true only when the mouse detects a movement.
  // this particular step ensures that all mouse changes are recorded.
  movement_flag = false;
}
